using System;
using System.IO;

namespace Mastermind
{
    /// <summary>
    /// Classe la qual contè els métodes del menu i els de les opcions d'aquest.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Mètode per a mostrar el menú per pantalla.
        /// </summary>
        public static void MostraMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("              /\\/\\   __ _ ___| |_ ___ _ __ _ __ ___ (_)_ __   __| |");
            Console.WriteLine("             /    \\ / _` / __| __/ _ \\ '__| '_ ` _ \\| | '_ \\ / _` |");
            Console.WriteLine("            / /\\/\\ \\ (_| \\__ \\ ||  __/ |  | | | | | | | | | | (_| |");
            Console.WriteLine("            \\/    \\/\\__,_|___/\\__\\___|_|  |_| |_| |_|_|_| |_|\\__,_|");
            Console.WriteLine("                                       MENU");
            Console.WriteLine("            1.Jugar");
            Console.WriteLine("            2.Ranking");
            Console.WriteLine("            0.Exit");
            Console.ResetColor();
        }
        /// <summary>
        /// Mètode per a triar una opcció del menú.
        /// </summary>
        /// <returns>Retorna booleà que en cas True, es tancara programa, en cas false, es continuarà executant aquest.</returns>
        public static bool OpcioMenu()
        {
            bool fi = false;
            string menu = Console.ReadLine();
            switch (menu)
            {
                case "1":
                    Master();
                    break;
                case "2":
                    InOutData.ReadPersonesRanking();
                    break;
                case "0":
                    fi = true;
                    break;
            }
            return fi;
        }
        /// <summary>
        /// Mètode que conté els mètodes fets servir al Mastermind, amb variables generals utilitzades al programa.
        /// </summary>
        public static void Master()
        {
            Console.Clear();
            InOutData.Inici();
            string nick = InOutData.Nick();
            string[] paraula = InOutData.Entrada_Lletres();
            string[] paradivi = InOutData.Adivina();
            int intents = 1;
            bool fidejoc=Mastermind.Comparacio(paraula,paradivi,ref intents);
            if (fidejoc)
            {
                InOutData.Win();
                InOutData.InputPersonaRanking(intents,nick);
                InOutData.ReadPersonesRanking();
            }

            if (!fidejoc)
            {
                InOutData.Lose();
                InOutData.ReadPersonesRanking();
            }
        }
        /// <summary>
        /// Mètode per a crear el fitxer on contindrà rànking, es posa el close perquè sinó no es tanca instància i surt sharing violation.
        /// </summary>
        public static void CreaRanking()
        {
            string fitxer = @"../../../MyDocs/Ranking.txt";
            if (!File.Exists(fitxer))
            {
                using (FileStream fs=File.Create(fitxer))
                {
                    fs.Close();
                }
            }
        }

        
    }
}