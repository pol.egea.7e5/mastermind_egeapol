namespace Mastermind
{
    /// <summary>
    /// Classe per a l'inicialització del programa.
    /// </summary>
    public class Inici
    {
        /// <summary>
        /// Mètode per a l'execució del menú i l'inicialització del programa.
        /// </summary>
        static void Main()
        {
            Menu.CreaRanking();
            do
            {
                Menu.MostraMenu();
            } while (!Menu.OpcioMenu());
        }
    }
}