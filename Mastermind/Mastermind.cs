﻿/*
 *  Author:Pol Egea
 *  Date: 16-11-2021
 * 
 */


namespace Mastermind
{
    /// <summary>
    /// Classe que conté la base del joc de Mastermind.
    /// </summary>
    public class Mastermind
    {
        /// <summary>
        /// Mètode per a la comparació entre la paraula introduida per l'usuari i la paraula a endevinar.
        /// </summary>
        /// <param name="paraula">Array de caràcters en string que representen la paraula introduida per l'usuari.</param>
        /// <param name="paradivi">Array de caràcters en string que representen la paraula a endevinar.</param>
        /// <param name="i">Enter que representa el número d'intents, el treiem fora ja que l'utilitzarem per al rànking.</param>
        /// <returns>Retorna booleà que en cas True s'ha guanyat, i que en cas false s'ha perdut.</returns>
        public static bool Comparacio(string[] paraula, string[] paradivi, ref int i)
        {
            string malposada = "";
            string benposada = "";
            bool lletraposada = false;
            bool fidejoc = false;
            string[] paraularepe = (string[]) paraula.Clone();
            string[] pararepetit = (string[]) paradivi.Clone();
            for (; fidejoc == false && i <= 12; i++)
            {
                for (int j = 0; j <= 3; j++)
                {
                    string lletraR = paraularepe[j];
                    for (int k = 0; k < 4; k++)
                    {
                        string lletraP = pararepetit[k];
                        if (pararepetit[j] == paraularepe[j] && lletraposada == false)
                        {
                            benposada += "*";
                            malposada += "-";
                            lletraposada = true;
                            pararepetit[j] = "";
                        }
                        else if (k == j && lletraP == lletraR && lletraposada == false)
                        {
                            benposada += "*";
                            malposada += "-";
                            lletraposada = true;
                            pararepetit[k] = "";
                        }
                        else if (k != j && lletraP == lletraR && lletraposada == false)
                        {
                            benposada += "-";
                            malposada += "*";
                            lletraposada = true;
                            pararepetit[k] = "";
                        }
                        else if (k == 3 && lletraposada == false)
                        {
                            benposada += "-";
                            malposada += "-";

                        }
                    }

                    lletraposada = false;

                }

                InOutData.Ubicacio(benposada, malposada);
                if (benposada == "****") fidejoc = true;
                if (fidejoc != true&&i<12)
                {
                    paraula = InOutData.Entrada_Lletres();
                    paraularepe = (string[]) paraula.Clone();
                    pararepetit = (string[]) paradivi.Clone();
                    malposada = "";
                    benposada = "";
                }
            }

            return fidejoc;
        }
    }
}