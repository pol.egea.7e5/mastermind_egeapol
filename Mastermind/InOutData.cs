using System;
using System.IO;

namespace Mastermind
{
    /// <summary>
    /// Classe per a la sortida i entrada de dades.
    /// </summary>
    public class InOutData
    {
        /// <summary>
        /// Classe per a la introducció de la paraula a adivinar.
        /// </summary>
        /// <returns>Retorna la paraula a adivinar.</returns>
        public static string[] Entrada_Lletres()
        {
            string[] paraula = new String[4];
            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Introdueixi quatre lletres que es trobin entre A i F (amb aquestes incloses)");
            Console.ResetColor();
            string paraulaformar = Console.ReadLine();
            while (!RevisaIntegritat(paraulaformar)||RevisaLletres(3,paraulaformar)!=4) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Paraula mal introduida");
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Introdueixi quatre lletres que es trobin entre A i F (amb aquestes incloses)");
                    Console.ResetColor();
                    paraulaformar = Console.ReadLine(); 
            }
            for (int z=0; z <= 3; z++)
            {
                if (paraulaformar != null) paraula[z] = Convert.ToString(paraulaformar[z]);
            }
            return paraula;
        }
        /// <summary>
        /// Mètode per a revisar el format de la paraula introduida a endevinar.
        /// </summary>
        /// <param name="paraulaformar">Paraula a endevinar.</param>
        /// <returns>Retorna booleà true en cas de que la paraula estigui correcte, false en cas contrari.</returns>
        public static bool RevisaIntegritat(string paraulaformar)
        {
            if (paraulaformar != null && paraulaformar.Length != 4) return false;
            if (paraulaformar is null) return false;
            return true;
        }
        /// <summary>
        /// Mètode recursiu per a revisar els caràcters utilitzats en la paraula a Endevinar.
        /// </summary>
        /// <param name="i">Representa la posició que ha d'agafar del string</param>
        /// <param name="paraulaformar">Paraula a endevinar.</param>
        /// <returns>Retorna 1 en cas de que el caràcter sigui correcte i salta al següent, en cas de que sigui incorrecte, o la posició i sigui -1 retorna 0 i s'atura la recursivitat.</returns>
        public static int RevisaLletres(int i,string paraulaformar)
        {
            if (i == -1) return 0;
            if ((paraulaformar[i] != 'A' && paraulaformar[i] != 'B' &&
                 paraulaformar[i] != 'C' && paraulaformar[i] != 'D' && paraulaformar[i] != 'E' &&
                 paraulaformar[i] != 'F')) return 0; 
            return 1 + RevisaLletres(i - 1,paraulaformar);

        }
        /// <summary>
        /// Mètode per a fer la paraula a endevinar.
        /// </summary>
        /// <returns>Retorna un string amb els caracters a endevinar.</returns>
        public static string[] Adivina()
        {
            string[] lletresPossibles = new string[] {"A", "B", "C", "D", "E", "F"};
            string[] paradivi = new String[4];
            for (int y = 0; y < paradivi.Length; y++)
            {
                Random rnd = new Random();
                paradivi[y] = lletresPossibles[rnd.Next(0, 6)];
            }

            return paradivi;
        }
        /// <summary>
        /// Mètode de sortida per en cas de Victoria.
        /// </summary>
        public static void Win()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Ha Guanyat");
            Console.ResetColor();
        }
        /// <summary>
        /// Mètode de sortida per en cas de derrota.
        /// </summary>
        public static void Lose()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ha Perdut");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Premi una tecla per sortir");
            Console.ResetColor();
        }
        /// <summary>
        /// Mètode de sortida de posicions correctes o incorrectes dels caràcters a endevinar.
        /// </summary>
        /// <param name="benposada">String que ubica els caràcters adivinats.</param>
        /// <param name="malposada">String que ubica els caràcters adivinats però que es troben mal colocats.</param>
        public static void Ubicacio(string benposada, string malposada)
        {
            Console.WriteLine("Lletres ben posades: " + benposada);
            Console.WriteLine("Lletres mal posades: " + malposada);
        }
        /// <summary>
        /// Mètode d'entrada d'un nick per al rànking.
        /// </summary>
        /// <returns>Retorna el nick introduit.</returns>
        public static string Nick()
        {
            string nom;
            do
            {
                Console.WriteLine("Introdueixi un nick o nom de jugador, DE 4 CARÀCTERS");
                nom = Console.ReadLine();
            } while (nom == null || nom.Length != 4);

            return nom;
        }
        /// <summary>
        /// Mètode per a l'introducció de dades al rànking.
        /// </summary>
        /// <param name="nick">Nom de l'usuari</param>
        /// <param name="intents">Enter que indica el nùmero d'intents que ha necessitat el/la jugador/a, menys intents corresponen a una probabilitat més alta d'entrar al rànking.</param>
        public static void InputPersonaRanking(int intents,string nick)
        {
            string fitxer = @"../../../MyDocs/Ranking.txt";
            string tmp = Path.GetTempFileName();
            using (StreamWriter sw = File.CreateText(tmp))
            using (StreamReader sr = File.OpenText(fitxer))
            {
                bool fet = false;
                for (int i = 1; i <= 10; i++)
                {
                    string s = sr.ReadLine();
                    if (s!=null)
                    {
                        string[] puntuacioS = s.Split(' ');
                        if (Convert.ToInt32(puntuacioS[1]) <= intents)
                        {
                            if (Convert.ToInt32(puntuacioS[1]) == intents && puntuacioS[0] == nick)
                            {
                                fet = true;
                            }
                            sw.WriteLine(s);
                        }
                        else if (intents < Convert.ToInt32(puntuacioS[1]) && !fet)
                        {
                            sw.WriteLine(nick+" "+intents);
                            sw.WriteLine(s);
                            fet = true;
                        }
                    }
                    else if (!fet)
                    {
                        sw.WriteLine(nick+" "+intents);
                        fet = true;
                    }
                }
            }
        
            File.Copy(tmp, fitxer, true);
        }
        /// <summary>
        /// Mètode per a llegir el rànking sencer.
        /// </summary>
        public static void ReadPersonesRanking()
        {
            Console.WriteLine("Ranking");
            string fitxer = @"../../../MyDocs/Ranking.txt";
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Nick NºIntents");
            Console.ResetColor();
            using (StreamReader sr = File.OpenText(fitxer))
            {
                string s;
                do
                {
                    s = sr.ReadLine();
                    Console.WriteLine(s);
                } while (s != null);
            }
            Console.WriteLine("Premi una tecla per a sortir");
            Console.ReadKey();
        }
        /// <summary>
        /// Mètode de sortida que dona la benvinguda al triar l'opció Mastermind.
        /// </summary>
        public static void Inici()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Benvingut al Mastermind");
            Console.ResetColor();
        }
    }
}