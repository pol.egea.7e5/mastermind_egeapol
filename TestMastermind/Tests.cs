﻿using Mastermind;
using NUnit.Framework;

namespace TestMastermind
{
    /// <summary>
    /// Classe per a poder fer tests
    /// </summary>
    [TestFixture]
    public class Tests
    {
        /// <summary>
        /// Mètode que revisa que el mètode Mastermind doni true.
        /// </summary>
        [Test]
        public void MastermindTrue()
        {
            int intent = 12;
            string[] paraulaIgual = {"A", "A", "A", "A"};
            Assert.True(Mastermind.Mastermind.Comparacio(paraulaIgual,paraulaIgual,ref intent));
        }
        /// <summary>
        /// Mètode que revisa que el mètode Mastermind doni false.
        /// </summary>
        [Test]
        public void MastermindFalse()
        {
            int intent = 12;
            string[] paraula1 = {"A", "A", "A", "A"};
            string[] paraula2 = {"B", "C", "A", "D"};
            Assert.False(Mastermind.Mastermind.Comparacio(paraula1,paraula2,ref intent));
        }
        /// <summary>
        /// Mètode que revisa que la paraula no utilitza l'abecedari correcte.
        /// </summary>
        [Test]
        public void ParaulaAbecedariIncorrecte()
        {
            string paraula1 = "AHAA";
            Assert.AreNotEqual(4,InOutData.RevisaLletres(3,paraula1));
        }
        /// <summary>
        /// Mètode que revisa que la paraula utilitza l'abecedari correcte.
        /// </summary>
        [Test]
        public void ParaulaAbecedariCorrecte()
        {
            string paraula1 = "AAAA";
            Assert.AreEqual(4,InOutData.RevisaLletres(3,paraula1));
        }
        /// <summary>
        /// Mètode que revisa que la paraula no utilitza la longitud correcte.
        /// </summary>
        [Test]
        public void ParaulaIntegritatIncorrecte()
        {
            string paraula1 = "AAAAA";
            Assert.False(InOutData.RevisaIntegritat(paraula1));
        }
        /// <summary>
        /// Mètode que revisa que la paraula utilitza la longitud correcte.
        /// </summary>
        [Test]
        public void ParaulaIntegritatCorrecte()
        {
            string paraula1 = "AAAA";
            Assert.True(InOutData.RevisaIntegritat(paraula1));
        }
    }
}